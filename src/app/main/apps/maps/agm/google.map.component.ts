import {Component, NgZone, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {MapsAPILoader, MouseEvent as AGMMouseEvent} from '@agm/core';
import {fuseAnimations} from '@fuse/animations';

import {GoogleMapService} from 'app/main/apps/maps/agm/google.map.service';
import {GoogleMapMarkerModel} from 'app/main/apps/maps/agm/google.map.marker.model';
import {GoogleMapConfig} from 'app/main/apps/maps/agm/google.map.config';
import {MatSnackBar, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'google-map',
    templateUrl: './google.map.component.html',
    styleUrls: ['./google.map.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class GoogleMapComponent implements OnInit, OnDestroy {

    private geoCoder;
    private _unsubscribeAll: Subject<any>;

    displayedColumns: string[] = ['index', 'label', 'latitude', 'longitude', 'action'];
    dataSource: MatTableDataSource<GoogleMapMarkerModel>;
    markers: GoogleMapMarkerModel[];
    latitude: number;
    longitude: number;

    markersAdd: GoogleMapMarkerModel[];
    markersRemove: number[];

    /**
     * Constructor
     *
     * @param {GoogleMapService} service
     */
    constructor(
        private config: GoogleMapConfig,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private service: GoogleMapService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.markersAdd = [];
        this.markersRemove = [];
        this.service.onMarkersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(markers => {
                if (markers) {
                    this.markers = markers;
                }
            });

        this.setCurrentLocation();

        this.dataSource = new MatTableDataSource<GoogleMapMarkerModel>(this.markers);

        // load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            this.setCurrentLocation();
            // @ts-ignore
            // tslint:disable-next-line:new-parens
            this.geoCoder = new google.maps.Geocoder;
        });
    }

    markerDragEnd($event: AGMMouseEvent): void {
        this.latitude = $event.coords.lat;
        this.longitude = $event.coords.lng;
        this.addMarker(this.latitude, this.longitude);

        this.dataSource._updateChangeSubscription();
    }

    save(): void {
        this.service.saveMarkers(this.markersAdd, this.markersRemove)
            .then((s) => {
                // Trigger the subscription with new data
                this.service.onMarkersChanged.next(s);
            });
    }

    removeMarker(label: string, index: number, id: number): void {
        this.markers.splice(index, 1);
        this.markersRemove.push(id);
        this.dataSource._updateChangeSubscription();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    // Get Current Location Coordinates
    private setCurrentLocation(): void {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
            });
        } else {
            this.latitude = 40.23;
            this.longitude = 44.23;
        }
    }

    private addMarker(latitude, longitude): void {
        this.geoCoder.geocode({location: {lat: latitude, lng: longitude}}, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    const data = new GoogleMapMarkerModel(null, latitude, longitude, results[0].formatted_address, true, null, 'info');
                    this.markers.push(data);
                    this.markersAdd.push(data);
                } else {
                    window.alert('No results found'); // TODO
                }
            } else {
                window.alert('Geocoder failed due to: ' + status); // TODO
            }
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

}
