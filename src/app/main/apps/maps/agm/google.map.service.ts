import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {GoogleMapMarkerModel} from './google.map.marker.model';

@Injectable()
export class GoogleMapService implements Resolve<any> {

    markers: any;
    onMarkersChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onMarkersChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getMarkers()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get Markers
     *
     * @returns {Promise<any>}
     */
    getMarkers(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(`${environment.api_url}/gis`)
                .subscribe((response: any) => {
                    this.markers = response;
                    this.onMarkersChanged.next(this.markers);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Save markers
     *
     * @param markers
     * @returns {Promise<any>}
     */
    saveMarkers(markers: GoogleMapMarkerModel[], removeIds: number[]): Promise<any> {
        const data = {
            markers : markers,
            removeIds: removeIds
        };
        return new Promise((resolve, reject) => {
            this._httpClient.put(`${environment.api_url}/gis`, data)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
