export class GoogleMapMarkerModel {

    id: number;
    latitude: number;
    longitude: number;
    label: string;
    draggable: boolean;
    icon: string;
    type: string;

    constructor(id: number,
                latitude: number,
                longitude: number,
                label: string,
                draggable?: boolean,
                icon?: string,
                type?: string) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.label = label;
        this.draggable = draggable;
        this.icon = icon;
        this.type = type;
    }
}
